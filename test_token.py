#!/usr/bin/env python
import os
import argparse

from TwinThreadUtils import * 

def main(debug):
    bearer = os.environ.get("TWIN_THREAD_TOKEN")
    url = os.environ.get("TWIN_THREAD_URL","https://dev.twinthread.com")
    username = os.environ.get("TWIN_THREAD_USER","sysadmin@canara.com")
    password = os.environ.get("TWIN_THREAD_PASSWORD","e*OgAY378z*M")

    twinThread = TwinThreadUtils(url,bearer,username,password,debug)
    
    # tags are: hardware, led_off, replaced, critical and trending

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d","--debug", help="Increase Output Verbosity", action="store_true")
    args = parser.parse_args()
    main(args.debug)     
   