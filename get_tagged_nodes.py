#!/usr/bin/env python
import mysql.connector
import os
import argparse

from TwinThreadUtils import * 

def main(debug):
    db_user = os.environ.get("MYSQL_AWS_USER")
    db_password = os.environ.get("MYSQL_AWS_PASSWORD")
    db_host = os.environ.get("MYSQL_AWS_HOST")
    db_name = os.environ.get("MYSQL_AWS_DB")
    
    try:
        conn = mysql.connector.connect(user=db_user, password=db_password, host=db_host, database=db_name, use_pure='True', charset='utf8')
    except mysql.connector.Error as e:
        print("Error:", e)
        return()
      
    cursor = conn.cursor(dictionary=True, buffered=True)
    
    # clear old entries    
#    try:
#        cursor.execute("delete from unit_status_test")
#        conn.commit()
#    except mysql.connector.IntegrityError as err:
#        if debug:
#            print("Error: {}".format(err))
#        conn.rollback() 
           
    bearer = os.environ.get("TWIN_THREAD_TOKEN")
    url = os.environ.get("TWIN_THREAD_URL","https://app.twinthread.com")
    username = os.environ.get("TWIN_THREAD_USER","sysadmin@canara.com")
    password = os.environ.get("TWIN_THREAD_PASSWORD","e*OgAY378z*M")

    twinThread = TwinThreadUtils(url,bearer,username,password,debug)
    
    # tags are: hardware, led_off, replaced, critical and trending
    node_cats = twinThread.load_tags()    
    
    if debug:
        print('\nshowing results')
    
    systems = get_migrated_systems(conn, debug) # get list of filenames that have been migrated to twin thread

    delete_unit_status = "delete from unit_status where filename in ('" + "','".join(systems) + "')" 
    
    if debug:
        print(delete_unit_status)
            
    try:
         
        cursor.execute(delete_unit_status)
        conn.commit()
    except mysql.connector.IntegrityError as err:
        if debug:
            print("Error: {}".format(err))
        conn.rollback() 
        
    for key in node_cats:
        
        for node in node_cats[key]:

            (filename, string_num, unit_num) = node.split(" / ")

            string_num = string_num.replace('String ','')
            unit_num = unit_num.replace('Unit ','')
            if debug:
                print("{},{},{},{}".format(key,filename,string_num,unit_num))

            if filename and filename in systems:
                add_unit_status(conn, filename, key, string_num, unit_num, debug)
            #else:
            #    print('could not find ' + filename)
                
    cursor.close()
    conn.close()      
    
#####
def get_migrated_systems(conn, debug):

    sql = "select a.filename from site a,site_parent b where a.site_parent=b.row_id and b.migrated=1 and a.system_model in ('TARMA','CANARA550')"

    if debug:        
        print(sql)  
                           
    cursor = conn.cursor() # not sure what buffered=True does
    cursor.execute(sql)
    systems = [i[0] for i in cursor.fetchall()]
    cursor.close()
    return(systems)


def add_unit_status(conn, filename, status, string_num, unit_num, debug):
         
    unit_sql = "INSERT INTO unit_status (filename, modified, status2, StrLbl, unit) VALUES (%s, curdate(), %s, %s, %s)"   
            
    unit_data = (filename, status, string_num, unit_num) 
        
    if debug:
        print(unit_data)
        
    try:
        cursor = conn.cursor()
        cursor.execute(unit_sql, unit_data)
        conn.commit()
    except mysql.connector.IntegrityError as err:
        if debug:
            print("Error: {}".format(err))
        conn.rollback()

    cursor.close()                            
   

    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d","--debug", help="Increase Output Verbosity", action="store_true")
    args = parser.parse_args()
    main(args.debug)     
