import requests
import json
import collections

class TwinThreadUtils:

    #  replaced is set to green, so it's not necessary
    #
    # '8': 'replaced',
    #
    tag_names = {
        #'6': 'hardware',
        '7': 'off',
        '9': 'critical',
        '10': 'trending'
    }
    
    def __init__(self, url, auth_token, username, password, debug = False, max_assets = 500):
        self.url = url
        self.auth_token = auth_token
        self.username = username
        self.password = password
        self.debug = debug
        self.max_assets = max_assets

        if auth_token is None:
            new_token = self.refresh_token()
            if self.debug:
                print(new_token['access_token'])
            self.auth_token = new_token['access_token']


    def refresh_token(self):

        endpoint = "/Token"
        payload = "grant_type=password&username=" + self.username + "&password=" + self.password
        #print(payload)
        
        headers = {
            'Content-Type': "application/x-www-form-urlencoded",
            'cache-control': "no-cache",
            'Postman-Token': "637eea41-145e-4bef-b248-efbe86f086d5"
            }    
        response = requests.request("POST", self.url+endpoint, data=payload, headers=headers)
        return json.loads(response.text)        
    
    def load_tags(self):
        
        results = collections.defaultdict(list)
        
        for tag in self.tag_names:
            if self.debug:
                print('loading ' + self.tag_names[tag])
                
            # fetch all nodes with a specific tag and get the associated system name
            tag_name = self.tag_names[tag]
            assets = []
            counter = 0 
            data = self.get_tags(tag, counter)
            
            # did we get any results back?
            if not data["results"]["assets"]:
                continue

            assets.extend(data["results"]["assets"])
            
            # paginate if total records exceeds max_assets
            totalCount = data["pageParams"]["count"]

            while len(assets) < totalCount:
                counter = counter + 1
                if self.debug:
                    print("counter {}, {}, {}".format(counter,len(assets),totalCount))

                data2 = self.get_tags(tag, counter * self.max_assets)
                assets.extend(data2["results"]["assets"])

            for asset in assets:
                node_details = self.get_asset_details(asset["assetId"])
                # the grandParent has the system name, which is user editable
                # I already found a case where the twin thread system name was different from ours
                # so I have to get the filename
                #
                system_details = self.get_asset_details(node_details["asset"][0]["grandParentId"])
                node_name = system_details["asset"][0]["friendlyId"] + ' / ' + asset["name"]
                results[tag_name].append(node_name)
                if self.debug:
                    print("{}, {}, {}".format(asset["assetId"],node_details["asset"][0]["grandParent"],node_name))        
        
        return results
        
    def get_tags(self, tag, skip = 0):

        # searching under system model tarma & canara550, classes: ["11", "19"], does not work
        # try battery node model
        endpoint = "/api/search/assetindex"
        payload = "{ classes: ['37','45'], tags: '" + str(tag) + "', maxassets:'" + str(self.max_assets) + "', skip:'" + str(skip) + "' }"
        #print(payload)
        
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + self.auth_token,
            'cache-control': "no-cache",
            'Postman-Token': "637eea41-145e-4bef-b248-efbe86f086d5"
            }    
        response = requests.request("POST", self.url+endpoint, data=payload, headers=headers)
        return json.loads(response.text)
        
        
    def get_asset_details(self, asset_id):
        
        endpoint = "/api/Asset/ListTabs"
        payload = "{ AssetId: '" + str(asset_id) + "' }" 
        
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + self.auth_token,
            'cache-control': "no-cache",
            'Postman-Token': "637eea41-145e-4bef-b248-efbe86f086d5"
            }    
        response = requests.request("POST", self.url+endpoint, data=payload, headers=headers)   
        return json.loads(response.text)